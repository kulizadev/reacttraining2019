const redux = require('redux');

const createstore = redux.createStore;

const initialstate = {
    counter: 0
}

const rootReducer = (state = initialstate, action) => {
   if(action.type == "INCREMENT_BY_ONE")
   {
        return {
            ...state,
            counter: state.counter + 1
        }
   }
   if(action.type == "INCREMENT_BY_VALUE")
   {
        return {
            ...state,
            counter: state.counter + action.value
        }
   }
   return state;
}

const store = createstore(rootReducer);
//console.log(store.getState());
store.dispatch({type: "INCREMENT_BY_ONE"});
store.dispatch({type: "INCREMENT_BY_VALUE", value: 10});
//console.log(store.getState());